package com.app.testapp.di

import com.app.testapp.repository.CommentsRepository
import org.koin.dsl.module
import org.koin.experimental.builder.single


val reposModule = module {

    single<CommentsRepository>()
}