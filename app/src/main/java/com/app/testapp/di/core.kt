package com.app.testapp.di

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.dsl.module

val coreModule =  httpModule + reposModule + viewModelModule + module {

    factory { CoroutineScope(Job() + Dispatchers.Default) }

}