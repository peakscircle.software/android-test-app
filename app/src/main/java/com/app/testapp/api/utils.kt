package com.app.testapp.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.HttpException
import java.net.SocketTimeoutException

sealed class NetworkResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : NetworkResult<T>()
    data class Error(val exception: Exception) : NetworkResult<Nothing>()
}

suspend fun <T : Any> safeApiResult(call: suspend () -> T): NetworkResult<T> {
    return try {
        val response = call.invoke()
        return NetworkResult.Success(response)
    } catch (e: HttpException) {
        e.printStackTrace()
        runCatching {
            e.response()?.errorBody()?.string()?.let {
                val body = ApiErrorJsonAdapter(
                    Moshi.Builder()
                        .add(KotlinJsonAdapterFactory())
                        .build()
                ).fromJson(it)
                return NetworkResult.Error(ServerError(body!!.message))
            }
        }
        NetworkResult.Error(e)
    } catch (e: SocketTimeoutException) {
        e.printStackTrace()
        NetworkResult.Error(e)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
        NetworkResult.Error(e)
    }
}

@JsonClass(generateAdapter = true)
data class ApiError(
    @Json(name = "error") val message: String
)

class ServerError(val localizedErrorMessage: String) : Exception()
