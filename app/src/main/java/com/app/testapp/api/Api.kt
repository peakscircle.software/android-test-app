package com.app.testapp.api

import com.app.testapp.api.response.Comment
import retrofit2.http.*

interface Api {

    @GET("comments/{id}")
    suspend fun getComments(@Path("id") id: Int): Comment

}