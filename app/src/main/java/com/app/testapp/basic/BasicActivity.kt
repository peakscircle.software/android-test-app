package com.app.testapp.basic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

abstract class BasicActivity<B : ViewDataBinding>(val layoutRes: Int)
    : AppCompatActivity() {

    lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBind()
    }

    private fun onBind() {
        binding = DataBindingUtil.setContentView(this, layoutRes)
    }

    fun <T> observe(liveData: LiveData<T>, observer: Observer<T>) {
        liveData.observe(this, observer)
    }

    fun <T> removeObserver(liveData: LiveData<T>, observer: Observer<T>) {
        liveData.removeObserver(observer)
    }

    protected fun bind(id: Int, variable: Any) {
        val isBound = binding.setVariable(id, variable)
        if (!isBound) {
            throw IllegalArgumentException("Binding failed! Incorrect binding variable name!")
        }
        binding.executePendingBindings()
    }
}
