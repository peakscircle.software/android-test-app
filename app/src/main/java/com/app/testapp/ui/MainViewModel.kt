package com.app.testapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.app.testapp.basic.BasicViewModel
import com.app.testapp.repository.CommentsRepository


class MainViewModel(private val commentsRepository: CommentsRepository): BasicViewModel() {

    val loading = MutableLiveData(false)

    private var minId = 0
    private var maxId = 0

    fun setCommentsIds(_minId: Int, _maxId: Int) {
        minId = _minId
        maxId = _maxId
    }

    val commentsFlow = Pager(PagingConfig(ITEMS_PER_PAGE)) {
        CommentsPagedDataSource(
            commentsRepository,
            minId,
            maxId
        )
    }.flow.cachedIn(scope)

    fun cancelGetCommentJob() {
        CommentsPagedDataSource.cancelGetCommentsJob()
        loading.postValue(false)
    }
}
