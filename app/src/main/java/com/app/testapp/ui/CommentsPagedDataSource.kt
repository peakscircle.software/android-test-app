package com.app.testapp.ui

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.testapp.api.NetworkResult
import com.app.testapp.api.response.Comment
import com.app.testapp.repository.CommentsRepository
import kotlinx.coroutines.*

const val ITEMS_PER_PAGE = 10

class CommentsPagedDataSource(
    private val commentsRepository: CommentsRepository,
    private val minId: Int,
    private val maxId: Int
) : PagingSource<Int, Any>() {

    private var currentIndex = 0

    companion object {
        private var deferredComments = CompletableDeferred<List<Comment>>()

        fun cancelGetCommentsJob() {
            deferredComments.cancel()
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Any>): Int? {
        return null
    }

    private fun setCurrentIndex(nextPageNumber: Int) {
        currentIndex = if(nextPageNumber == 0) minId
        else minId + nextPageNumber * ITEMS_PER_PAGE
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Any> {
        if(minId != 0) {
            val nextPageNumber = params.key ?: 0
            setCurrentIndex(nextPageNumber)

            deferredComments = CompletableDeferred()
            coroutineScope {
                val comments = arrayListOf<Comment>()
                val commentsJob = ArrayList<Deferred<NetworkResult<Comment>>>()

                for (index in 0 until ITEMS_PER_PAGE) {
                    if (currentIndex <= maxId) {
                        commentsJob.add(
                            async {
                                val id = if(nextPageNumber == 0) minId + index
                                else  minId + index + nextPageNumber * ITEMS_PER_PAGE
                                commentsRepository.getComments(id)
                            }
                        )
                        ++currentIndex
                    } else break
                }

                for (index in 0 until commentsJob.size) {
                    commentsJob[index].await().apply {
                        if (this is NetworkResult.Success) {
                            comments.add(data)
                        }
                    }
                }

                deferredComments.complete(comments)
            }

            val commentsList = deferredComments.await().sortedBy { it.id }
            val nextPage = if (commentsList.size < ITEMS_PER_PAGE) null
            else nextPageNumber + 1

            return LoadResult.Page(commentsList, null, nextPage)
        }
        else {
            return LoadResult.Page(listOf(), null, null)
        }
    }
}