package com.app.testapp.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.app.testapp.R
import com.app.testapp.basic.BasicActivity
import com.app.testapp.databinding.ActivityMainBinding
import com.app.testapp.ext.getIntValue
import com.app.testapp.ext.hideKeyboard
import com.app.testapp.ext.show
import com.app.testapp.ui.adapter.CommentsAdapter
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity: BasicActivity<ActivityMainBinding>(
    R.layout.activity_main
) {
    private val viewModel by viewModel<MainViewModel>()
    private lateinit var commentsAdapter: CommentsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTextListeners()
        setOnClickListeners()
        initObservers()
    }

    private fun initAdapter() {
        commentsAdapter = CommentsAdapter()
        binding.commentsRecyclerview.adapter = commentsAdapter

        launchFundsFlow()

        lifecycleScope.launchWhenCreated {
            commentsAdapter.loadStateFlow.collectLatest {
                val isRefreshing = it.refresh is LoadState.Loading
                showLoader(isRefreshing)
                if(isRefreshing) { setEmptyView(false) }
                else setEmptyView(commentsAdapter.itemCount < 1)
            }
        }
    }

    private fun getMinBound(): Int {
        val firstBoundIndex = binding.firstBoundInput.getIntValue()
        val secondBoundIndex = binding.secondBoundInput.getIntValue()

        return if(secondBoundIndex > firstBoundIndex) firstBoundIndex
        else secondBoundIndex
    }

    private fun getMaxBound(): Int {
        val firstBoundIndex = binding.firstBoundInput.getIntValue()
        val secondBoundIndex = binding.secondBoundInput.getIntValue()

        return if(secondBoundIndex > firstBoundIndex) secondBoundIndex
        else firstBoundIndex
    }

    private fun setEmptyView(isEmpty: Boolean) {
        binding.commentsRecyclerview.show(isEmpty.not())
        binding.emptyText.show(isEmpty)
    }

    private fun initObservers() {
        observe(viewModel.loading) {
            showLoader(it)
        }
    }

    private fun showLoader(isLoading: Boolean) {
        binding.layoutProgress.root.show(isLoading)
    }

    private fun setOnClickListeners() {
        binding.getDataButton.setOnClickListener {
            onGetDataClicked()
        }
        binding.layoutProgress.cancelButton.setOnClickListener {
            viewModel.cancelGetCommentJob()
        }
    }

    private fun onGetDataClicked() {
        if(this::commentsAdapter.isInitialized.not())
            initAdapter()
        viewModel.setCommentsIds(
            getMinBound(),
            getMaxBound()
        )
        hideKeyboard()
        showLoader(true)
        commentsAdapter.refresh()
    }

    private fun launchFundsFlow() {
        lifecycleScope.launchWhenCreated {
            viewModel.commentsFlow.collect {
                commentsAdapter.submitData(it)
            }
        }
    }

    private fun initTextListeners() {
        val editTexts = listOf(binding.firstBoundInput, binding.secondBoundInput)
        for (editText in editTexts) {
            editText.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    //delete zero or minus symbol if it the first symbol
                    if (s.toString() == "0" || s.toString() == "-") {
                        editText.removeTextChangedListener(this)
                        editText.setText("")
                        editText.addTextChangedListener(this)
                    }
                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int, count: Int, after: Int
                ) {
                    /* do nothing */
                }

                override fun afterTextChanged(s: Editable) {
                    /* do nothing */
                }
            })
        }
    }
}