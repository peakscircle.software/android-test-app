package com.app.testapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.testapp.api.response.Comment
import com.app.testapp.databinding.ItemCommentBinding

class CommentsAdapter: PagingDataAdapter<Any, RecyclerView.ViewHolder>(BillingsDiff) {

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else if (payloads.contains(PAYLOAD_COLLAPSE)) {
            localBindViewHolder(holder, position)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        localBindViewHolder(holder, position)
    }

    private fun localBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val comment = getItem(position) as Comment
        (holder as CommentItemHolder).bind(comment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CommentItemHolder(
            ItemCommentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    class CommentItemHolder(val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.comment = comment
        }
    }

    object BillingsDiff : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(
            oldItem: Any,
            newItem: Any
        ): Boolean = when {
            oldItem is Comment && newItem is Comment -> oldItem.id == newItem.id
            else -> false
        }

        override fun areContentsTheSame(
            oldItem: Any,
            newItem: Any
        ): Boolean = when {
            oldItem is Comment && newItem is Comment -> oldItem as Comment == newItem
            else -> false
        }
    }

    companion object {
        private const val PAYLOAD_COLLAPSE: Int = 28
    }
}