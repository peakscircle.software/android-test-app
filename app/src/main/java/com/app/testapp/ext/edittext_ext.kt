package com.app.testapp.ext

import android.widget.EditText

fun EditText.getIntValue() = text.toString().toInt().or(0)
