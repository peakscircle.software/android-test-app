package com.app.testapp.repository

import com.app.testapp.api.Api
import com.app.testapp.api.safeApiResult

class CommentsRepository(private val api: Api) {

    suspend fun getComments(commentId: Int) =
        safeApiResult { api.getComments(commentId) }
}